package bf.isge.ic3.todo.service;

import bf.isge.ic3.todo.model.Todo;
import bf.isge.ic3.todo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TodoServiceJPAImpl implements TodoService{
    private TodoRepository todoRepository;
    @Autowired
    public TodoServiceJPAImpl(TodoRepository todoRepository){
        this.todoRepository = todoRepository;
    }
    @Override
    public Todo createTodo(Todo todo) {
        return todoRepository.save(todo);
    }

    @Override
    public Todo updateTodo(Todo todo) {
        return todoRepository.save(todo);
    }

    @Override
    public Todo findTodoById(Integer id) {
        return todoRepository.findById(id).orElseThrow();
    }

    @Override
    public boolean archiveTodo(Integer id) {
        Todo todo = findTodoById(id);
        todo.setArchived(true);
        todoRepository.save(todo);
        return true;
    }

    @Override
    public List<Todo> findAllTodo() {
        List<Todo> result = new ArrayList<>();
        Iterable<Todo> all = todoRepository.findAll();
        all.forEach(result::add);
        return result;
    }

    @Override
    public void deleteTodo(Integer id) {
        todoRepository.deleteById(id);
    }

    @Override
    public Map<LocalDate, Integer> findTodoCountPerDay() {
        return null;
    }
}
