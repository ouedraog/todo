package bf.isge.ic3.todo.service;

import bf.isge.ic3.todo.model.Todo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TodoServiceImpl implements TodoService{
    Map<Integer, Todo> allTodo = new HashMap<>();
    @Override
    public Todo createTodo(Todo todo) {
        todo.setId(allTodo.size());
        todo.setCreatedAt(LocalDate.now());
        allTodo.put(todo.getId(), todo);
        return todo;
    }

    @Override
    public Todo updateTodo(Todo todo) {
        allTodo.put(todo.getId(), todo);
        return todo;
    }

    @Override
    public Todo findTodoById(Integer id) {
        return allTodo.get(id);
    }

    @Override
    public boolean archiveTodo(Integer id) {
        return false;
    }

    @Override
    public List<Todo> findAllTodo() {
        return new ArrayList<>(allTodo.values());
    }

    @Override
    public void deleteTodo(Integer id) {
        allTodo.remove(id);
    }

    @Override
    public Map<LocalDate, Integer> findTodoCountPerDay() {
        return null;
    }
}
