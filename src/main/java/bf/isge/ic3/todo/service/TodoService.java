package bf.isge.ic3.todo.service;

import bf.isge.ic3.todo.model.Todo;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface TodoService {
    /**
     * Create a toto
     * @param todo the todo to create without id and createdDate
     * @return created todo
     */
    Todo createTodo(Todo todo);
    Todo updateTodo(Todo todo);
    Todo findTodoById(Integer id);
    boolean archiveTodo(Integer id);
    List<Todo> findAllTodo();
    void deleteTodo(Integer id);
    Map<LocalDate, Integer> findTodoCountPerDay();
}
