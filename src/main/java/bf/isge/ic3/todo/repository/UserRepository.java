package bf.isge.ic3.todo.repository;

import bf.isge.ic3.todo.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {
    List<User> findByName(String name);
}
