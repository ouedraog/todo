package bf.isge.ic3.todo.repository;

import bf.isge.ic3.todo.model.Todo;
import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<Todo, Integer> {

}
