package bf.isge.ic3.todo.controller.web;

import bf.isge.ic3.todo.controller.web.form.TodoForm;
import bf.isge.ic3.todo.model.Todo;
import bf.isge.ic3.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TodoPageController {
    // Injecté via application.properties.
    @Value("${error.message}")
    private String errorMessage;

    @Autowired
    private TodoService todoService;

    @RequestMapping(value = { "/todoList" }, method = RequestMethod.GET)
    public String todoList(Model model) {
        model.addAttribute("todoList", todoService.findAllTodo());
        return "todoList";
    }

    @RequestMapping(value = { "/addTodo" }, method = RequestMethod.GET)
    public String showAddPersonPage(Model model) {
        TodoForm todoForm = new TodoForm();
        model.addAttribute("todoForm", todoForm);
        return "addTodo";
    }

    @RequestMapping(value = { "/addTodo" }, method = RequestMethod.POST)
    public String savePerson(Model model,
                             @ModelAttribute("todoForm") TodoForm todoForm) {

        String title = todoForm.getTitle();
        String description = todoForm.getDescription();

        if ( (title != null && !title.isBlank()  &&
                (description !=null && !description.isBlank()))) {
            Todo todo = new Todo();
            todo.setTitle(title);
            todo.setDescription(description);
            todoService.createTodo(todo);
            return "redirect:/todoList";
        }
        model.addAttribute("errorMessage", errorMessage);
        return "addTodo";
    }
}
