package bf.isge.ic3.todo.controller.rest;

import bf.isge.ic3.todo.model.Todo;
import bf.isge.ic3.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController()
@RequestMapping("/api/todos")
public class TodoRestController {
    @Autowired
    TodoService todoService;

    @PostMapping(consumes = {"application/json"},
            produces = {"application/json"}, path = "")
    public Todo createTodo(@RequestBody Todo todo) {
        return this.todoService.createTodo(todo);
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "")
    public List<Todo> findAllTodo(){
        return this.todoService.findAllTodo();
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "byId")
    public Todo findTodoById(@RequestParam("id") Integer id){
        return this.todoService.findTodoById(id);
    }

    @DeleteMapping("{id}")
    public void deleteTodo(@PathVariable Integer id){
        this.todoService.deleteTodo(id);
    }

}
