package bf.isge.ic3.todo.controller.web.form;

import lombok.Data;

@Data
public class TodoForm {
    private String title;
    private String description;
}
