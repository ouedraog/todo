package bf.isge.ic3.todo.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class Todo {
    @Id
    @GeneratedValue
    private Integer id;
    private String title;
    private String description;
    @ManyToOne(fetch = FetchType.EAGER)
    private User author;
    private LocalDate createdAt;
    @Column(columnDefinition = "boolean default false")
    private Boolean archived;

    @PrePersist
    protected void onCreate() {
        this.createdAt = LocalDate.now();
    }
}
