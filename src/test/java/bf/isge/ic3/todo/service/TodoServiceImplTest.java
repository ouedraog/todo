package bf.isge.ic3.todo.service;

import bf.isge.ic3.todo.model.Todo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TodoServiceImplTest {

    @Test
    void test_createTodo() {
        TodoService todoService = new TodoServiceImpl();
        Todo todo1 = new Todo();
        todo1.setTitle("todo 1");
        todo1.setDescription("desc todo1");
        Todo createdTodo = todoService.createTodo(todo1);
        Assertions.assertEquals("todo 1", createdTodo.getTitle());
        Assertions.assertEquals("desc todo1", createdTodo.getDescription());
        Assertions.assertNotNull(createdTodo.getId());
        Assertions.assertNotNull(createdTodo.getCreatedAt());
        Assertions.assertEquals(1, todoService.findAllTodo().size() );
    }
    @Test
    void test_deleteToto(){
        TodoService todoService = new TodoServiceImpl();
        Todo todo1 = new Todo();
        todo1.setTitle("todo 1");
        todo1.setDescription("desc todo1");
        Todo createdTodo = todoService.createTodo(todo1);
        Assertions.assertEquals(1, todoService.findAllTodo().size() );
        todoService.deleteTodo(createdTodo.getId());
        Assertions.assertEquals(0, todoService.findAllTodo().size() );

    }
}